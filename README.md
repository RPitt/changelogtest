# ChangeLogTest

Testing the [GitLab changelog generation API](https://docs.gitlab.com/ee/api/repositories.html#generate-changelog-data)

This repo contains a [`.gitlab-ci.yml`](.gitlab-ci.yml) that invokes the API above to update the [`CHANGELOG.md`](CHANGELOG.md) after every commit to master.  
The version number assigned in the `.gitlab-ci.yml` is derived from `$CI_PIPLINE_IID` and a tag is also created at the same point.  

The in-built GitLab changelog generator parses commit messages ending in `Changelog: xxx` where `xxx` is a category.
Categories can be whatever you like. You can however define a mapping of the categories to longer/friendlier/more consistent catagory descriptions by using the [`.gitlab/changelog_config.yml`](.gitlab/changelog_config.yml)

### Problems/annoyances/disappointments
- Because the generator is parsing commit messages, usage of Squashed commits is very problematic as commit data is removed.
  - If all the relevant changelog data from the commits is missing then the changelog get's updated with 'No changes'.
  - One possible mitigator is to edit the squashed commit manually, but this is tedious and error prone.
- The alternative assumption is that you know at commit time exactly how your change should be described, and that there will be no further evoution of that prior to merge.
  - It's often hard at the start of a sequence of work to know that, you might have to rebase/rewrite your commits later on.
- There doesn't appear to be anyway to 'update' the CHANGELOG without creating a new `##` release heading/date in the CHANGELOG. 
- Version increment is entirely in the hands of the user, that might arguably be a good thing for some, but I had expected GitLab to offer at least some opionated optional option out of the box.



