## 0.1.7 (2021-04-05)

### Fixes (1 change)

- [Update README.md, let's pretend this is a fix.](RPitt/changelogtest@c147b7948b9bd940dd103018276e1324143b7ae4)

## 0.1.6 (2021-04-05)

### Documentation (1 change)

- [Another readme update.](RPitt/changelogtest@a013aeef9471f163847a973a224d9fe150549710)

## 0.1.5 (2021-04-05)

### Documentation (1 change)

- [Update README.md](RPitt/changelogtest@67b96b044c8d1915b831337e5e641031e69f0dcf) ([merge request](RPitt/changelogtest!2))

## 0.1.4 (2021-04-05)

No changes.

## 0.1.3 (2021-04-05)

### Features (1 change)

- [Testing the changelog generator. Direct commit to master.](RPitt/changelogtest@1e9b3901ffe1d823800c23f3ebb55ea00c9c57e0)

## 0.1.2 (2021-04-05)

No changes.
